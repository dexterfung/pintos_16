#ifndef VM_SWAP_H
#define VM_SWAP_H

#include "devices/block.h"
#include "threads/vaddr.h"
#include "vm/frame.h"

#include <stddef.h>
#include <stdbool.h>
#include <hash.h>


#define SECTOR_PER_PAGE PGSIZE / BLOCK_SECTOR_SIZE

void swap_init (void);
void write_to_swap (struct frame_entry *f);
void remove_from_swap (struct frame_entry *f, struct frame_entry *new_f, void *ker_address);

int insert_swap_entry (struct frame_entry *f);
struct frame_entry * get_swap_entry (struct hash_elem *e);
struct frame_entry * get_swap_entry_from_page (void *upage);
bool delete_swap_entry (struct hash_elem *e);

#endif
