#ifndef VM_PAGE_H
#define VM_PAGE_H

#include <hash.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <debug.h>
#include "userprog/process.h"

#define NON_SWAP 0

struct sup_page_table
{
    // corresponding process
    struct proc *proc;
    // hash of struct frame_entry that are in swap space
    struct hash page_hash;

    // initialised data segment of the process's virtual memory
    void *data;
    // code segment of the process's virtual memory
    void *code;

    struct lock page_lock;

    struct hash_elem h_elem;
};

enum memory_data_type
{
    MMF = 0x01,
    SWAP = 0x02,
    EXECUTABLE = 0x04,
    MAPPED = 0x08
};

struct sup_page_entry
{
    // key
    void *user_addr;
    struct hash_elem spe_elem;

    // EXEC, SWAP, MMF, MAPPED
    enum memory_data_type data_type;

    // structure needed for loading a file, MMF
    struct file *executable;
    uint32_t read_bytes;
    uint32_t zero_bytes;
    bool is_write;
    off_t offset;

    // SWAP
    uint32_t swap_id;

    // MAPPED
    void *ker_addr;
};

struct alias
{
    tid_t tid;
    void *user_addr;
    struct list_elem alias_elem;
};

void page_init(struct sup_page_table *spt);
unsigned page_hash(const struct hash_elem *f_, void *aux UNUSED);
bool page_less(const struct hash_elem *a_, const struct hash_elem *b_, void *aux UNUSED);
bool insert_page_entry(struct hash_elem *e, struct sup_page_table *spt);
struct sup_page_entry *get_page_entry(struct hash_elem *e, struct sup_page_table *spt);
struct sup_page_entry *get_page_entry_from_page(void *user_addr, struct sup_page_table *spt);
struct sup_page_entry *get_page_entry_from_kpage(void *ker_addr, struct sup_page_table *spt);
bool delete_page_entry(struct hash_elem *e, struct sup_page_table *spt);
void page_destroy(struct sup_page_table *spt);
bool spt_set_page(struct sup_page_table *spt, void *upage, void *kpage);
bool spt_clear_page(struct sup_page_table *spt, void *upage, bool swapped);
// mmf
bool load_executable(struct sup_page_entry *pe);
bool load_swap(struct sup_page_entry *pe);
bool load_data(struct sup_page_entry *pe);

bool create_spe(void *user_addr, struct file *file, uint32_t read_bytes, uint32_t zero_bytes, bool is_write, off_t offset,
                enum memory_data_type type, uint32_t swap_id,
                void *ker_addr);
bool create_spe_exec(void *user_addr, struct file *file, uint32_t read_bytes, uint32_t zero_bytes, bool is_write, off_t offset);
bool create_spe_swap(void *user_addr, uint32_t swap_id, void *kern_addr);
bool create_spe_mapped(void *user_addr, void *ker_addr);
bool create_spe_unmapped(void *user_addr, bool is_write);
/* for stack growth */
bool grow_stack(struct sup_page_entry *spe);
bool grow_stack_sc(void *addr);

#endif
