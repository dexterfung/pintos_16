#include "vm/frame.h"
#include "threads/thread.h"
#include "threads/synch.h"
#include "threads/malloc.h"
#include "devices/timer.h"
#ifdef USERPROG
#include "userprog/process.h"
#include "userprog/pagedir.h"
#endif
#ifdef VM
#include "vm/swap.h"
#include "vm/page.h"
#endif
#include <debug.h>
#include <list.h>

#define RESET_TICKS 100

// obscured global frame table
static struct hash frame_table;
// synchronisation
static struct lock frame_lock;

unsigned frame_hash(const struct hash_elem *f_, void *aux UNUSED);
bool frame_less(const struct hash_elem *a_, const struct hash_elem *b_, void *aux UNUSED);
void unset_assessed(struct hash_elem *e, void *aux UNUSED);
void reset_assessed(void);

/******************** frame table functions **********************/
/* Returns a hash value for page p */
unsigned
frame_hash(const struct hash_elem *f_, void *aux UNUSED)
{
    const struct frame_entry *f = hash_entry(f_, struct frame_entry, h_elem);
    return hash_bytes(&f->ker_addr, sizeof f->ker_addr);
}

/* Returns true if page a precedes page b */
bool frame_less(const struct hash_elem *a_, const struct hash_elem *b_,
                void *aux UNUSED)
{
    const struct frame_entry *a = hash_entry(a_, struct frame_entry, h_elem);
    const struct frame_entry *b = hash_entry(b_, struct frame_entry, h_elem);
    return a->ker_addr < b->ker_addr;
}

/* initiate the global frame table */
void frame_init(void)
{
    lock_init(&frame_lock);
    hash_init(&frame_table, frame_hash, frame_less, NULL);
}

/* return true if successfully inserted */
bool insert_frame_entry(struct frame_entry *f)
{
    lock_acquire(&frame_lock);
    struct hash_elem *e = &(f->h_elem);
    bool ret = (hash_insert(&frame_table, e) == NULL);
    lock_release(&frame_lock);
    return ret;
}

/* get the corresponding frame entry to hash_elem e */
struct frame_entry *
get_frame_entry(struct hash_elem *e)
{
    lock_acquire(&frame_lock);
    struct hash_elem *h_elem = hash_find(&frame_table, e);
    lock_release(&frame_lock);
    if (h_elem)
    {
        struct frame_entry *f = hash_entry(h_elem, struct frame_entry, h_elem);
        return f;
    }
    else
    {
        return NULL;
    }
}

/* get the corresponding frame entry to PAGE */
struct frame_entry *
get_frame_entry_from_page(void *ker_addr)
{
    struct hash_iterator i;
    hash_first(&i, &frame_table);
    struct frame_entry *f = NULL;
    lock_acquire(&frame_lock);
    while (hash_next(&i))
    {
        struct frame_entry *tmp = hash_entry(hash_cur(&i), struct frame_entry, h_elem);
        if (tmp->ker_addr == ker_addr)
        {
            f = tmp;
            break;
        }
    }
    lock_release(&frame_lock);
    return f;
}

/* Finds, removes, and returns the corresponding element
   if successfully deleted, else return NULL */
struct frame_entry *
delete_frame_entry(struct hash_elem *e)
{
    lock_acquire(&frame_lock);
    struct hash_elem *h_elem = hash_delete(&frame_table, e);
    lock_release(&frame_lock);
    return hash_entry(h_elem, struct frame_entry, h_elem);
}

struct frame_entry *
delete_frame_entry_from_page(void *ker_addr)
{
    struct hash_iterator i;
    hash_first(&i, &frame_table);
    struct frame_entry *f = NULL;
    while (hash_next(&i))
    {
        f = hash_entry(hash_cur(&i), struct frame_entry, h_elem);
        if (f->ker_addr == ker_addr)
        {
            break;
        }
    }
    if (f)
    {
        lock_acquire(&frame_lock);
        hash_delete(&frame_table, &f->h_elem);
        lock_release(&frame_lock);
        return f;
    }
    else
    {
        return NULL;
    }
}

/* find a victim frame_entry using the second chance
   replacement algorithm, doesn't remove it from frame table */
struct frame_entry *
find_frame_second_chance(void)
{
    struct hash_iterator i;
    hash_first(&i, &frame_table);
    struct frame_entry *f = NULL;
    lock_acquire(&frame_lock);

    bool found = false;
    while (hash_next(&i))
    {
        f = hash_entry(hash_cur(&i), struct frame_entry, h_elem);
        if (f->is_pinned)
        {
            printf("jumped\n");
            continue;
        }
        /* iterate through the aliases to see if it has been recently accessed */
        bool accessed = false;
        struct list_elem *e;
        lock_acquire(&f->alias_lock);
        for (e = list_begin(&f->alias_list); e != list_end(&f->alias_list); e = list_next(e))
        {
            struct alias *a = list_entry(e, struct alias, alias_elem);
            uint32_t *pd = thread_from_tid(a->tid)->pagedir;
            if (pagedir_is_accessed(pd, a->user_addr))
            {
                accessed = true;
                pagedir_set_accessed(pd, a->user_addr, false);
            }
        }
        if (!accessed)
        {
            found = true;
            lock_release(&f->alias_lock);
            break;
        }
        lock_release(&f->alias_lock);
    }
    /* if the whole table has been recently accessed
       then return the first (oldest) entry */
    if (!found)
    {
        hash_first(&i, &frame_table);
        f = hash_entry(hash_cur(&i), struct frame_entry, h_elem);
    }

    // delete it from frame table
    hash_delete(&frame_table, &f->h_elem);

    // insert into swap
    int swap_id = insert_swap_entry(f);
    if (swap_id < 0)
    {
        printf("error - cannot insert to swap\n");
    }

    write_to_swap(f);

    // update list of alias that the mapping is cleared
    struct list_elem *e;
    for (e = list_begin(&f->alias_list); e != list_end(&f->alias_list); e = list_next(e))
    {
        struct alias *a = list_entry(e, struct alias, alias_elem);
        struct thread *t = thread_from_tid(a->tid);
        if (t)
        {
            
            uint32_t *pd = t->pagedir;
            
            pagedir_clear_page(pd, a->user_addr);
            // printf ("segfsdgs235653475474526347548769879jg7dg\n");
            struct sup_page_table *spt = t->spt;
            // printf ("segfsdgs235653475474526347548769mmnbbbmbvbvmnvvnbv879jg7dg\n");
            if (!spt_clear_page(spt, a->user_addr, true))
            {
                printf("cannot clear mapping in spt during eviction\n");
            }
        }
        else
        {
            printf("no thread found\n");
        }
    }

    lock_release(&frame_lock);
    return f;
}

/********************* frame entry functions ***********************/
struct frame_entry *
frame_entry_create(void *ker_addr, void *user_addr)
{
    struct frame_entry *f = (struct frame_entry *)malloc(sizeof(struct frame_entry));
    if (!f)
    {
        printf("error - cannot malloc for frame_entry\n");
    }
    f->first_sector_index = -1;
    f->ker_addr = ker_addr;
    f->swap_id = 0;
    f->is_pinned = false;
    list_init(&f->alias_list);
    lock_init(&f->alias_lock);
    struct alias *a = (struct alias *)malloc(sizeof(struct alias));
    if (!a)
    {
        printf("error in creating alias\n");
        return NULL;
    }
    a->tid = thread_current()->tid;
    a->user_addr = user_addr;
    list_push_back(&f->alias_list, &a->alias_elem);
    return f;
}

void frame_entry_destroy(struct frame_entry *f)
{
    // don't free page because page will be allocate to another frame entry
    f->ker_addr = NULL;
    while (!list_empty(&f->alias_list))
    {
        struct list_elem *e = list_pop_front(&f->alias_list);
        struct alias *a = list_entry(e, struct alias, alias_elem);
        free(a);
    }
    free(f);
}

struct frame_entry *
hash_to_frame_entry(const struct hash_elem *e)
{
    return hash_entry(e, struct frame_entry, h_elem);
}

void debug_frame_entry(struct frame_entry *f)
{
    printf("frame entry : %p\n", f);
    printf("ker_addr: %p\n", f->ker_addr);
    printf("swap_id: %u\n", f->swap_id);
    printf("h_elem: %p\n", &f->h_elem);
}

void debug_frame_table(void)
{
    struct hash_iterator i;
    hash_first(&i, &frame_table);

    while (hash_next(&i))
    {
        struct frame_entry *f = hash_entry(hash_cur(&i), struct frame_entry, h_elem);
        debug_frame_entry(f);
        printf("\n");
    }
}