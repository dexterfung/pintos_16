#include "vm/page.h"
#include "vm/frame.h"
#include "vm/swap.h"
#include "threads/synch.h"
#include "threads/palloc.h"
#include "threads/vaddr.h"
#include "threads/thread.h"
#include "threads/malloc.h"
#include "userprog/pagedir.h"
#include "userprog/process.h"
#include "userprog/exception.h"
#include <debug.h>

/* Returns a hash value for page p */
unsigned
page_hash(const struct hash_elem *s_, void *aux UNUSED)
{
    const struct sup_page_entry *s = hash_entry(s_, struct sup_page_entry, spe_elem);
    return hash_bytes(&s->user_addr, sizeof(s->user_addr));
}

/* Returns true if page a precedes page b */
bool page_less(const struct hash_elem *a_, const struct hash_elem *b_,
               void *aux UNUSED)
{
    const struct sup_page_entry *a = hash_entry(a_, struct sup_page_entry, spe_elem);
    const struct sup_page_entry *b = hash_entry(b_, struct sup_page_entry, spe_elem);
    return a->user_addr < b->user_addr;
}

void page_init(struct sup_page_table *spt)
{
    // init fields in struct sup_page_table
    hash_init(&(spt->page_hash), page_hash, page_less, NULL);
    lock_init(&spt->page_lock);
}

void free_spe(struct hash_elem *e_, void *aux UNUSED);

void free_spe(struct hash_elem *e_, void *aux UNUSED)
{
    struct sup_page_entry *e = hash_entry(e_, struct sup_page_entry, spe_elem);
    free(e);
}

void page_destroy(struct sup_page_table *spt)
{
    lock_acquire(&spt->page_lock);
    hash_destroy(&spt->page_hash, free_spe);
    lock_release(&spt->page_lock);
    free(spt);
}

/* return true if successfully inserted */
bool insert_page_entry(struct hash_elem *e, struct sup_page_table *spt)
{
    lock_acquire(&spt->page_lock);
    bool ret = hash_insert(&spt->page_hash, e) == NULL;
    lock_release(&spt->page_lock);
    return ret;
}

/* get the corresponding frame entry to hash_elem e */
struct sup_page_entry *
get_page_entry(struct hash_elem *e, struct sup_page_table *spt)
{
    lock_acquire(&spt->page_lock);
    struct hash_elem *e_found = hash_find(&spt->page_hash, e);
    lock_release(&spt->page_lock);
    if (e_found)
    {
        return hash_entry(e_found, struct sup_page_entry, spe_elem);
    }
    return NULL;
}

/* get the corresponding frame entry to PAGE */
struct sup_page_entry *
get_page_entry_from_page(void *user_addr, struct sup_page_table *spt)
{
    struct hash_iterator i;
    hash_first(&i, &spt->page_hash);
    struct sup_page_entry *spe = NULL;
    lock_acquire(&spt->page_lock);
    while (hash_next(&i))
    {
        struct sup_page_entry *tmp = hash_entry(hash_cur(&i), struct sup_page_entry, spe_elem);
        if (!tmp)
        {
            break;
        }
        if (tmp->user_addr == user_addr)
        {
            spe = tmp;
            break;
        }
    }
    lock_release(&spt->page_lock);
    return spe;
}

/* return true if successfully deleted */
bool delete_page_entry(struct hash_elem *e, struct sup_page_table *spt)
{
    lock_acquire(&spt->page_lock);
    bool ret = hash_delete(&spt->page_hash, e) != NULL;
    lock_release(&spt->page_lock);
    return ret;
}

bool load_executable(struct sup_page_entry *pe)
{

    file_seek(pe->executable, pe->offset);

    void *page = vm_palloc_get_page(PAL_USER, pe->user_addr);

    if (page == NULL)
    {
        printf("cannot get page - eviction failed\n");
        return false;
    }

    struct frame_entry *f = get_frame_entry_from_page(page);
    f->is_pinned = true;
    lock_acquire(&filesys_lock);
    bool is_read = file_read(pe->executable, page, pe->read_bytes) == (int)pe->read_bytes;
    lock_release(&filesys_lock);
    if (!is_read)
    {

        vm_palloc_free_page(page);
        return false;
    }

    memset(page + pe->read_bytes, 0, pe->zero_bytes);

    if (!install_page(pe->user_addr, page, pe->is_write))
    {
        printf("UNSUCCESSFULLY LOADED\n");
        vm_palloc_free_page(page);
        return false;
    }
    spt_set_page(thread_current()->spt, pe->user_addr, page);
    f->is_pinned = false;
    return true;
}

bool load_swap(struct sup_page_entry *pe)
{
    printf("swapping\n\n");
    file_seek(pe->executable, pe->offset);

    void *page = vm_palloc_get_page(PAL_USER, pe->user_addr);

    if (!page)
    {
        return false;
    }
    // write from swap to frame
    struct frame_entry *new_f = get_frame_entry_from_page(page);
    struct frame_entry *f = get_swap_entry_from_page(pe->user_addr);
    new_f->is_pinned = true;
    remove_from_swap(f, new_f, page);
    new_f->is_pinned = false;
    // update aliases spt and pagedir
    struct list_elem *e;
    for (e = list_begin(&new_f->alias_list); e != list_end(&new_f->alias_list); e = list_next(e))
    {
        struct alias *a = list_entry(e, struct alias, alias_elem);
        uint32_t *pd = thread_from_tid(a->tid)->pagedir;
        pagedir_set_page(pd, a->user_addr, page, pe->is_write);
        struct sup_page_table *spt = thread_from_tid(a->tid)->spt;
        spt_set_page(spt, a->user_addr, page);
    }
    return true;
}

bool load_data(struct sup_page_entry *pe)
{

    enum memory_data_type type = pe->data_type;

    switch (type)
    {
    case SWAP:
        return load_swap(pe);
        break;
    case MMF:
    case EXECUTABLE:
        return load_executable(pe);
        break;
    default:
        return false;
    }
}

bool create_spe_exec(void *user_addr, struct file *file, uint32_t read_bytes, uint32_t zero_bytes, bool is_write, off_t offset)
{
    return create_spe(user_addr, file, read_bytes, zero_bytes, is_write,
                      offset, EXECUTABLE, NON_SWAP, NULL);
}

bool create_spe_swap(void *user_addr, uint32_t swap_id, void *ker_addr)
{
    return create_spe(user_addr, NULL, 0, 0, false, 0, SWAP, swap_id, ker_addr);
}

bool create_spe_mapped(void *user_addr, void *ker_addr)
{
    return create_spe(user_addr, NULL, 0, 0, false, 0, MAPPED, 0, ker_addr);
}

bool create_spe_unmapped(void *user_addr, bool is_write)
{
    return create_spe(user_addr, NULL, 0, 0, is_write, 0, 0, 0, NULL);
}

bool create_spe(void *user_addr, struct file *file, uint32_t read_bytes, uint32_t zero_bytes, bool is_write, off_t offset,
                enum memory_data_type type, uint32_t swap_id,
                void *ker_addr)
{

    struct sup_page_entry *pe = malloc(sizeof(struct sup_page_entry));

    if (!pe)
    {
        return false;
    }

    pe->user_addr = user_addr;
    pe->data_type = type;
    pe->executable = file;
    pe->read_bytes = read_bytes;
    pe->zero_bytes = zero_bytes;
    pe->is_write = is_write;
    pe->offset = offset;
    pe->swap_id = swap_id;
    pe->ker_addr = ker_addr;

    struct sup_page_table *spt = thread_current()->spt;

    //insert the the entry into the table's page hash
    if (!insert_page_entry(&pe->spe_elem, spt))
    {
        free(pe);
        return false;
    }

    return true;
}

// set a mapping between uaddr and kaddr in spt, uaddr must be already in spt
bool spt_set_page(struct sup_page_table *spt, void *upage, void *kpage)
{
    struct sup_page_entry *spe = get_page_entry_from_page(upage, spt);
    if (!spe)
    {
        return false;
    }
    spe->ker_addr = kpage;
    spe->data_type = spe->data_type | MAPPED;
    return true;
}

// clear a mapping between uaddr and kaddr in spt, uaddr must be already in spt
bool spt_clear_page(struct sup_page_table *spt, void *upage, bool swapped)
{
    struct sup_page_entry *spe = get_page_entry_from_page(upage, spt);
    if (!spe)
    {
        return false;
    }
    spe->ker_addr = NULL;
    spe->data_type = spe->data_type & ~MAPPED; // todo check
    if (swapped)
    {
        spe->data_type = spe->data_type | SWAP;
    }
    return true;
}

bool grow_stack(struct sup_page_entry *spe)
{

    void *upage = spe->user_addr;
    return grow_stack_sc(upage);
}

bool grow_stack_sc(void *addr)
{

    bool success = false;
    void *kpage;
    void *upage;

    //move the stack pointer downward with PGSIZE then allocate the additional page
    upage = addr;

    kpage = vm_palloc_get_page(PAL_USER | PAL_ZERO, upage);

    if (kpage == NULL)
    {
        return false;
    }

    if (PHYS_BASE - upage <= STACK_LIMIT)
    {
        success = install_page(pg_round_down(upage), kpage, true);

        if (!success)
        {
            vm_palloc_free_page(kpage);
            return false;
        }
    }
    else
    {
        vm_palloc_free_page(kpage);
        return false;
    }

    return true;
}
