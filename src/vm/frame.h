#ifndef VM_FRAME_H
#define VM_FRAME_H

#include "devices/timer.h"
#include "devices/block.h"
#include "userprog/process.h"
#include <hash.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <list.h>

#ifdef VM
#include "vm/page.h"
#endif

#define SET     1
#define UNSET   0

struct frame_entry
{
    // pointer of kernel address
    void *ker_addr;
    // pointer of user address
    uint32_t swap_id;
    // hash table element for frame table or swap table
    struct hash_elem h_elem;
    // list of alias
    struct list alias_list;
    struct lock alias_lock;

    block_sector_t first_sector_index;

    bool is_pinned;
};

// typedef struct frame_entry frame_entry;

void frame_init (void);
bool insert_frame_entry (struct frame_entry *f);
struct frame_entry * get_frame_entry (struct hash_elem *e);
struct frame_entry * get_frame_entry_from_page (void *ker_addr);
struct frame_entry * delete_frame_entry (struct hash_elem *e);
struct frame_entry * delete_frame_entry_from_page (void *ker_addr);
struct frame_entry * find_frame_second_chance (void);

/****** frame_entry function ********/
struct frame_entry * frame_entry_create (void *ker_addr, void *user_addr);
void frame_entry_destroy (struct frame_entry *);
struct frame_entry * hash_to_frame_entry (const struct hash_elem *e);

void * frame_entry_get_user_addr (const struct frame_entry *f);
void frame_entry_set_user_addr (struct frame_entry *f, void *user_addr);

void debug_frame_entry (struct frame_entry *f);
void debug_frame_table (void);
#endif