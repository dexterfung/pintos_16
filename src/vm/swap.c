#include "vm/swap.h"
#include "vm/frame.h"
#include "devices/block.h"
#include "threads/synch.h"
#include "threads/malloc.h"
#include "filesys/filesys.h"
#include <debug.h>
#include <hash.h>
#include <bitmap.h>

// synchronisation
static struct lock swap_lock;
// need a swap disk
static struct block *swap_block;
// Bitmap to store free swap places
static struct bitmap *swap_bitmap;
// Hash table to store the frame_entry
static struct hash swap_table;
static uint32_t id = 0;
block_sector_t swap_size = 0;

/* Returns a hash value for page p */
static unsigned
swap_hash(const struct hash_elem *f_, void *aux UNUSED)
{
    const struct frame_entry *f = hash_to_frame_entry(f_);
    return hash_bytes(&f->ker_addr, sizeof f->ker_addr);
}

/* Returns true if page a precedes page b */
static bool
swap_less(const struct hash_elem *a_, const struct hash_elem *b_,
          void *aux UNUSED)
{
    const struct frame_entry *a = hash_to_frame_entry(a_);
    const struct frame_entry *b = hash_to_frame_entry(b_);
    return a->ker_addr < b->ker_addr;
}

/* initiate the global swap table */
void swap_init(void)
{
    /* Initialise the swap block device */
    swap_block = block_get_role(BLOCK_SWAP);

    if (!swap_block)
    {
        PANIC("Failed to initialise swap block device!");
    }

    /* Initialise the bitmap. */
    swap_size = block_size((struct block *)swap_block);
    swap_bitmap = bitmap_create(8192);

    if (!swap_bitmap)
    {
        PANIC("Failed to initialise bitmap for swap.");
    }

    /* Initialise the hash table for frame_entry. */
    hash_init(&swap_table, swap_hash, swap_less, NULL);

    /* Initialise the lock. */
    lock_init(&swap_lock);
}

void write_to_swap(struct frame_entry *f)
{
    lock_acquire(&swap_lock);

    

    bool is_full = bitmap_all(swap_bitmap, 0, swap_size);

    void *page = f->ker_addr;

    if (is_full)
    {
        PANIC("The swag device is full!");
    }

    block_sector_t first_free_sector = bitmap_scan_and_flip(swap_bitmap, 0, SECTOR_PER_PAGE, false);

    if (first_free_sector == BITMAP_ERROR)
    {
        return;
    }

    f->first_sector_index = (block_sector_t)first_free_sector;

    for (int i = 0; i < SECTOR_PER_PAGE; i++)
    {
        block_write(swap_block, first_free_sector + i, page + BLOCK_SECTOR_SIZE * i);
    }

    lock_release(&swap_lock);
}

/* remove frame entry F associated data from swap and wrtie to physical memory KER_ADDRESS */
void remove_from_swap(struct frame_entry *f, struct frame_entry *new_f, void *ker_address) //todo check if ker_addr
{
    lock_acquire(&swap_lock);

    block_sector_t first_sector_index = f->first_sector_index;

    for (int i = 0; i < SECTOR_PER_PAGE; i++)
    {
        block_read(swap_block, first_sector_index + i, ker_address + BLOCK_SECTOR_SIZE * i);
    }

    bitmap_set_multiple(swap_bitmap, first_sector_index, SECTOR_PER_PAGE, false);

    f->first_sector_index = (block_sector_t)-1;

    lock_release(&swap_lock);

    new_f->alias_list = f->alias_list;
    free(f);
}

/* return non-negative int if successfully inserted */
int insert_swap_entry(struct frame_entry *f)
{
    lock_acquire(&swap_lock);
    ++id;
    f->swap_id = id;
    int ret = (int)id;
    if (hash_insert(&swap_table, &f->h_elem) == NULL)
    {
        lock_release(&swap_lock);
        return ret;
    }

    lock_release(&swap_lock);
    return -1;
}

/* get the corresponding frame entry to hash_elem e */
struct frame_entry *
get_swap_entry(struct hash_elem *e)
{
    struct hash_elem *found = hash_find(&swap_table, e);

    //If not found, return null pointer.
    if (found == NULL)
    {
        printf("NOT FOUND.");
        return NULL;
    }
    struct frame_entry *f_entry = hash_to_frame_entry(e);
    return f_entry;
}

struct frame_entry *
get_swap_entry_from_page(void *upage)
{
    struct hash_iterator i;
    hash_first(&i, &swap_table);
    struct frame_entry *f = NULL;
    lock_acquire(&swap_lock);
    bool found = false;
    while (hash_next(&i))
    {
        f = hash_entry(hash_cur(&i), struct frame_entry, h_elem);
        /* iterate through the aliases to see if it has been recently accessed */
        struct list_elem *e;
        lock_acquire(&f->alias_lock);
        for (e = list_begin(&f->alias_list); e != list_end(&f->alias_list); e = list_next(e))
        {
            struct alias *a = list_entry(e, struct alias, alias_elem);
            if (a->user_addr == upage)
            {
                found = true;
                break;
            }
        }
    }
    lock_release(&swap_lock);
    if (found)
    {
        return f;
    }
    else
    {
        return NULL;
    }
}

/* return true if successfully deleted */
bool delete_swap_entry(struct hash_elem *e)
{
    lock_acquire(&swap_lock);
    if (hash_delete(&swap_table, e) == NULL)
    {
        lock_release(&swap_lock);
        return false;
    }

    lock_release(&swap_lock);
    return true;
}
