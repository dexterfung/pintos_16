#ifndef THREADS_FIXEDPOINT_H
#define THREADS_FIXEDPOINT_H

#include <stdint.h>

/*
  Functions that help calculating Fixed-point real arithmetic
  In 17.14 fixed-point format (p.q format)

  p = 17, q = 14 ; p + q = 31;let f = 2^q, i.e. 2^14

  1. From real(n) to p.q format:
	n * f = n*2^14
  2. From p.q format(x) to real:
	if(x >= 0) (x + f/2)/f
        else       (x - f/2)/f
*/

#define P_FIXPT 17	        /*p = 17 in p.q fixed point format*/
#define Q_FIXPT 14		      /*q = 14 in p.q fixed point format*/
#define F (1 << Q_FIXPT)  	/*let f = 2^q ie f = 2^14*/

/* convert int to fixed point */
#define real_to_fixpt(N) (N * F)

/*convert real num to fixed point(round zero)*/
#define fixpt_to_real_round_zero(X) (X / F)

/*convert real num to fixed point (nearest)*/
#define fixpt_to_real_round_nearest(X) \
(X >= 0 ? ((X + F/2) / F) : ((X - F/2) / F))



/*Adding 2 fixed points*/
#define fixpt_add(X, Y) (X + Y)

/*Subtracting 2 fixed points*/
#define fixpt_sub(X, Y) (X - Y)

/*multiplying 2 fixed points*/
#define fixpt_mult(X, Y) (((int64_t) X) * Y / F)

/*dividing 2 fixed points*/
#define fixpt_div(X, Y) (((int64_t) X) * F / Y)



/*Adding fixed point to real*/
#define fixpt_real_add(X, N) (X + N * F)

/*Subtracting real from fixed point*/
#define fixpt_real_sub(X, N) (X - N * F)

/*Multiplying fixed point and real*/
#define fixpt_real_mult(X, N) (X * N)

/*Dividing real from fixed point*/
#define fixpt_real_div(X, N) (X / N)



#endif /* threads/fixed-point.h */
