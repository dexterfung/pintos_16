#include "threads/thread.h"
#include <debug.h>
#include <stddef.h>
#include <random.h>
#include <stdio.h>
#include <string.h>
#include "threads/flags.h"
#include "threads/interrupt.h"
#include "threads/intr-stubs.h"
#include "threads/palloc.h"
#include "threads/switch.h"
#include "threads/synch.h"
#include "threads/vaddr.h"
#include "threads/fixed-point.h"

#ifdef USERPROG
#include "userprog/process.h"
#endif

#ifdef VM
#include "vm/page.h"
#include "vm/frame.h"
#endif

/* Random value for struct thread's `magic' member.
   Used to detect stack overflow.  See the big comment at the top
   of thread.h for details. */
#define THREAD_MAGIC 0xcd6abf4b

/* List of processes in THREAD_READY state, that is, processes
   that are ready to run but not actually running. */
static struct list ready_list;

/* List of all processes.  Processes are added to this list
   when they are first scheduled and removed when they exit. */
static struct list all_list;

/* Idle thread. */
static struct thread *idle_thread;

/* Initial thread, the thread running init.c:main(). */
static struct thread *initial_thread;

/* Lock used by allocate_tid(). */
static struct lock tid_lock;

/* Advanced scheduler */
int load_avg;

/* Stack frame for kernel_thread(). */
struct kernel_thread_frame
  {
    void *eip;                  /* Return address. */
    thread_func *function;      /* Function to call. */
    void *aux;                  /* Auxiliary data for function. */
  };

/* Statistics. */
static long long idle_ticks;    /* # of timer ticks spent idle. */
static long long kernel_ticks;  /* # of timer ticks in kernel threads. */
static long long user_ticks;    /* # of timer ticks in user programs. */

/* Scheduling. */
#define TIME_SLICE 4            /* # of timer ticks to give each thread. */
static unsigned thread_ticks;   /* # of timer ticks since last yield. */

/* If false (default), use round-robin scheduler.
   If true, use multi-level feedback queue scheduler.
   Controlled by kernel command-line option "-o mlfqs". */
bool thread_mlfqs;

static void kernel_thread (thread_func *, void *aux);

static void idle (void *aux UNUSED);
static struct thread *running_thread (void);
static struct thread *next_thread_to_run (void);
static void init_thread (struct thread *, const char *name, int priority);
static bool is_thread (struct thread *) UNUSED;
static void *alloc_frame (struct thread *, size_t size);
static void schedule (void);
void thread_schedule_tail (struct thread *prev);
static tid_t allocate_tid (void);

/* Initializes the threading system by transforming the code
   that's currently running into a thread.  This can't work in
   general and it is possible in this case only because loader.S
   was careful to put the bottom of the stack at a page boundary.

   Also initializes the run queue and the tid lock.

   After calling this function, be sure to initialize the page
   allocator before trying to create any threads with
   thread_create().

   It is not safe to call thread_current() until this function
   finishes. */
void
thread_init (void)
{
  ASSERT (intr_get_level () == INTR_OFF);

  lock_init (&tid_lock);
  list_init (&ready_list);
  list_init (&all_list);

  /* Set up a thread structure for the running thread. */
  initial_thread = running_thread ();
  init_thread (initial_thread, "main", PRI_DEFAULT);
  initial_thread->status = THREAD_RUNNING;
  initial_thread->tid = allocate_tid ();


}

/* Starts preemptive thread scheduling by enabling interrupts.
   Also creates the idle thread. */
void
thread_start (void)
{
  /* Create the idle thread. */
  struct semaphore idle_started;
  sema_init (&idle_started, 0);
  thread_create ("idle", PRI_MIN, idle, &idle_started);

  /* Initialise load_avg. */
  load_avg = real_to_fixpt(0);

  /* Start preemptive thread scheduling. */
  intr_enable ();

  /* Wait for the idle thread to initialize idle_thread. */
  sema_down (&idle_started);

  /* assign a process to the initial_thread */
  process_init (initial_thread);  
}

/* Called by the timer interrupt handler at each timer tick.
   Thus, this function runs in an external interrupt context. */
void
thread_tick (void)
{
  struct thread *t = thread_current ();

  /* Update statistics. */
  if (t == idle_thread)
    idle_ticks++;
#ifdef USERPROG
  else if (t->pagedir != NULL)
    user_ticks++;
#endif
  else
    kernel_ticks++;

  /* Enforce preemption. */
  if (++thread_ticks >= TIME_SLICE)
    intr_yield_on_return ();
}

/* Prints thread statistics. */
void
thread_print_stats (void)
{
  printf ("Thread: %lld idle ticks, %lld kernel ticks, %lld user ticks\n",
          idle_ticks, kernel_ticks, user_ticks);
}

/* Creates a new kernel thread named NAME with the given initial
   PRIORITY, which executes FUNCTION passing AUX as the argument,
   and adds it to the ready queue.  Returns the thread identifier
   for the new thread, or TID_ERROR if creation fails.

   If thread_start() has been called, then the new thread may be
   scheduled before thread_create() returns.  It could even exit
   before thread_create() returns.  Contrariwise, the original
   thread may run for any amount of time before the new thread is
   scheduled.  Use a semaphore or some other form of
   synchronization if you need to ensure ordering.

   The code provided sets the new thread's `priority' member to
   PRIORITY, but no actual priority scheduling is implemented.
   Priority scheduling is the goal of Problem 1-3. */
tid_t
thread_create (const char *name, int priority,
               thread_func *function, void *aux)
{
  struct thread *t;
  struct kernel_thread_frame *kf;
  struct switch_entry_frame *ef;
  struct switch_threads_frame *sf;
  tid_t tid;
  enum intr_level old_level;

  ASSERT (function != NULL);

  /* Allocate thread. */
  t = palloc_get_page (PAL_ZERO);
  if (t == NULL)
    return TID_ERROR;

  /* Initialize thread. */
  init_thread (t, name, priority);
  tid = t->tid = allocate_tid ();

  #ifdef USERPROG
  // set up parent
  t -> parent_thread = thread_current ();
  // init a corresponding process for t
  process_init (t);

  #endif

  #ifdef VM
  t -> spt = (struct sup_page_table *) malloc (sizeof (struct sup_page_table));
  page_init (t -> spt);
  #endif

  /* Prepare thread for first run by initializing its stack.
     Do this atomically so intermediate values for the 'stack'
     member cannot be observed. */
  old_level = intr_disable ();

  /* Stack frame for kernel_thread(). */
  kf = alloc_frame (t, sizeof *kf);
  kf->eip = NULL;
  kf->function = function;
  kf->aux = aux;

  /* Stack frame for switch_entry(). */
  ef = alloc_frame (t, sizeof *ef);
  ef->eip = (void (*) (void)) kernel_thread;

  /* Stack frame for switch_threads(). */
  sf = alloc_frame (t, sizeof *sf);
  sf->eip = switch_entry;
  sf->ebp = 0;

  intr_set_level (old_level);

  /* Add to run queue. */
  thread_unblock (t);

  /* check if the newly create thread has higher priority than
     current running thread */
  if (t-> effective_pri > thread_get_priority() && !thread_mlfqs) {
    thread_yield();
  }

  return tid;
}

/* Puts the current thread to sleep.  It will not be scheduled
   again until awoken by thread_unblock().

   This function must be called with interrupts turned off.  It
   is usually a better idea to use one of the synchronization
   primitives in synch.h. */
void
thread_block (void)
{
  ASSERT (!intr_context ());
  ASSERT (intr_get_level () == INTR_OFF);

  thread_current ()->status = THREAD_BLOCKED;
  schedule ();
}

/* Transitions a blocked thread T to the ready-to-run state.
   This is an error if T is not blocked.  (Use thread_yield() to
   make the running thread ready.)

   This function does not preempt the running thread.  This can
   be important: if the caller had disabled interrupts itself,
   it may expect that it can atomically unblock a thread and
   update other data. */
void
thread_unblock (struct thread *t)
{
  enum intr_level old_level;

  ASSERT (is_thread (t));

  old_level = intr_disable ();
  ASSERT (t->status == THREAD_BLOCKED);
  list_insert_ordered (&ready_list, &t->elem,
                       (list_less_func *) is_higher_priority, NULL);
  t->status = THREAD_READY;
  intr_set_level (old_level);
}

/* Returns the name of the running thread. */
const char *
thread_name (void)
{
  return thread_current ()->name;
}

/* Returns the running thread.
   This is running_thread() plus a couple of sanity checks.
   See the big comment at the top of thread.h for details. */
struct thread *
thread_current (void)
{
  struct thread *t = running_thread ();

  /* Make sure T is really a thread.
     If either of these assertions fire, then your thread may
     have overflowed its stack.  Each thread has less than 4 kB
     of stack, so a few big automatic arrays or moderate
     recursion can cause stack overflow. */
  ASSERT (is_thread (t));
  ASSERT (t->status == THREAD_RUNNING);

  return t;
}

/* Returns the running thread's tid. */
tid_t
thread_tid (void)
{
  return thread_current ()->tid;
}

/* Deschedules the current thread and destroys it.  Never
   returns to the caller. */
void
thread_exit (void)
{
  ASSERT (!intr_context ());

#ifdef USERPROG
// printf("debug5\n");

  process_exit ();

  
  // printf("debug6\n");
#endif

  #ifdef VM
  // printf("before free in VM \n");  
  
  page_destroy (thread_current() -> spt);
  // printf("after free in VM \n\n");
  #endif
  /* Remove thread from all threads list, set our status to dying,
     and schedule another process.  That process will destroy us
     when it calls thread_schedule_tail(). */
  intr_disable ();
  list_remove (&thread_current()->allelem);
  thread_current ()->status = THREAD_DYING;
  schedule ();
  NOT_REACHED ();

  
}

/* Yields the CPU.  The current thread is not put to sleep and
   may be scheduled again immediately at the scheduler's whim. */
void
thread_yield (void)
{
  struct thread *cur = thread_current ();
  enum intr_level old_level;

  ASSERT (!intr_context ());

  old_level = intr_disable ();
  if (cur != idle_thread)
   /* list_push_back (&ready_list, &cur->elem); */
      list_insert_ordered (&ready_list, &cur->elem, (list_less_func *) is_higher_priority, NULL);

  cur->status = THREAD_READY;
  schedule ();
  intr_set_level (old_level);
}

/* Invoke function 'func' on all threads, passing along 'aux'.
   This function must be called with interrupts off. */
void
thread_foreach (thread_action_func *func, void *aux)
{
  struct list_elem *e;

  ASSERT (intr_get_level () == INTR_OFF);

  for (e = list_begin (&all_list); e != list_end (&all_list);
       e = list_next (e))
    {
      struct thread *t = list_entry (e, struct thread, allelem);
      func (t, aux);
    }
}

/* Sets the current thread's priority to NEW_PRIORITY. */
void
thread_set_priority (int new_priority)
{
  ASSERT(new_priority >= PRI_MIN && new_priority <= PRI_MAX);

  if (thread_mlfqs) {
    return;
  }

  enum intr_level old_level;
  old_level = intr_disable ();

  thread_current ()->priority = new_priority;
  set_effective_pri(thread_current(), NEST_PRI);

  /* check if current thread still has the highest priority */
  struct list_elem *e;
  e = list_begin(&ready_list);
  struct thread *t = list_entry(e,struct thread, elem);

  if (t -> effective_pri > thread_current() -> effective_pri) {
     thread_yield();
  }

  intr_set_level (old_level);
}

/* set the effective priority of thread, set recursively to donee
   if necessary */
void set_effective_pri (struct thread *t, int i) {
  if (thread_mlfqs) {
    return;
  }

enum intr_level old_level;
  old_level = intr_disable ();

  if (list_empty (&t->donor_list)) {
    t->effective_pri = t->priority;
  }else {
    /* refresh donors order */
    list_sort (&t->donor_list,
               (list_less_func *) donor_higher_priority, NULL);

    struct list_elem *e;
    e = list_begin (&t->donor_list);
    struct thread *td = list_entry (e, struct thread, donor_elem);

    /* compare first donor's effective priority with current thread's
       (base) priority */
    if (td->effective_pri > t->priority) {
      t->effective_pri = td->effective_pri;
    } else {
      t->effective_pri = t->priority;}
  }

  if (t->status == THREAD_READY) {
   list_sort (&ready_list, (list_less_func *) is_higher_priority, NULL);
  }

  /* recursively set the effective priority to donee if necessary */
  if (t->donee_lock != NULL) {
    list_sort (&t->donee_lock->semaphore.waiters,
                (list_less_func *) is_higher_priority, NULL);
    if (i > 0) {
      set_effective_pri (t->donee_lock->holder, i - 1);
    }
  }

  intr_set_level (old_level);
}

/* Returns the current thread's priority. */
int
thread_get_priority (void)
{
  return thread_current ()->effective_pri;
}

/* Sets the current thread's nice value to NICE. */
void
thread_set_nice (int new_nice)
{
  /* Check that the input value is valid, i.e. between -20 and 20. */
  ASSERT (new_nice <= NICE_MAX && new_nice >= NICE_MIN);

  /* Update the niceness of the thread. */
  thread_current()->nice = new_nice;

  /* Recalculate the priority of the thread based on the new niceness. */
  thread_cal_priority(thread_current());

  /* If ready_list is empty, it means that there is no running thread. */
  if (list_empty(&ready_list)) {
   return ;
  }

  /* Getting the running thread. */
  struct list_elem *e;
  e = list_begin(&ready_list);
  struct thread *t = list_entry(e, struct thread, elem);

  /* If the running thread no longer has the highest priority, yields. */
  if (t->effective_pri > thread_current()->effective_pri) {
    thread_yield();
  }
}

/* Returns the current thread's nice value. */
int
thread_get_nice (void)
{
  return thread_current()->nice;
}

/* Recalculate the priority of giving thread. */
void
thread_cal_priority (struct thread *t)
{

  /* Do NOT calculate the priority if the thread is in idle. */
  if (t == idle_thread) {
    return ;
  }

  /* This function only works when thread_mlfqs is TRUE. */
  ASSERT (thread_mlfqs);

  /* Calculation of the new priority value. */
  int32_t sndT = fixpt_real_div (t->recent_cpu, 4);
  int32_t trdT = t->nice * 2;

  int32_t pri_max = real_to_fixpt (PRI_MAX);
  int32_t lastT   = fixpt_real_add (sndT, trdT);


  /* Round down the priority value */
  int pri = fixpt_to_real_round_zero (fixpt_sub (pri_max, lastT));

  /* The calculated priority is adjusted to lie in the valid range
     PRI_MIN to PRI_MAX. */
  if (pri < PRI_MIN) {
    t->effective_pri = PRI_MIN;
  } else if (pri > PRI_MAX) {
    t->effective_pri = PRI_MAX;
  } else {
    t->effective_pri = pri;
  }

}

/* Returns 100 times the system load average. */
int
thread_get_load_avg (void)
{
  int result = fixpt_real_mult(load_avg, 100);
  /* Rounded to the nearest integer. */
  return fixpt_to_real_round_nearest(result);
}

/* Recalculate the value of load_avg. */
void
thread_cal_load_avg (void)
{
  /* This function only works when thread_mlfqs is TRUE. */
  ASSERT (thread_mlfqs);

  /* Store the numbner of ready threads */
  size_t ready_threads = list_size(&ready_list);

  /* Increment read_threads by one if the current thread is not in idle. */
  if (thread_current() != idle_thread) {
    ready_threads++;
  }

  /* Calculation of load_avg */
  int32_t coeff_fstT = real_to_fixpt (59);
  int32_t coeff_fst  = fixpt_real_div (coeff_fstT, 60);

  int32_t fstT = fixpt_mult (coeff_fst, load_avg);
  int32_t sndT = fixpt_real_div (real_to_fixpt(ready_threads), 60);
  load_avg = fixpt_add (fstT, sndT);

}

/* Returns 100 times the current thread's recent_cpu value. */
int
thread_get_recent_cpu (void)
{
  int result = fixpt_real_mult(thread_current()->recent_cpu, 100);
  /* Rounded to the nearest integer. */
  return fixpt_to_real_round_nearest(result);
}

/* Recalculate and update the value of recent_cpu for EVERY threads. */
void
thread_cal_recent_cpu (void)
{
  /* This function only works when thread_mlfqs is TRUE. */
  ASSERT (thread_mlfqs);

  struct thread *t;
  struct list_elem *e = list_begin(&all_list);

  /* Run through all the threads and calculate the recent_cpu respectively */
  for (; e != list_end(&all_list); e = list_next (e))
  {
    t = list_entry(e, struct thread, allelem);
    /* Only update the recent_cpu value if the thread is NOT in idle. */
    if (t != idle_thread) {

      /* Calculation of recent_cpu of the thread. */
      int32_t numer = fixpt_real_mult(load_avg, 2);
      int32_t denom = fixpt_real_add(numer, 1);
      int32_t r_cpu = t->recent_cpu;
      int32_t fstT  = fixpt_mult (fixpt_div(numer, denom), r_cpu);

      int n = t->nice;
      t->recent_cpu = fixpt_real_add (fstT, n);

      /* Recalculate thje priority based on the updated recent_cpu value */
      thread_cal_priority(t);
    }
  }

}

/* Increment the value of recent_cpu of current thread by one. */
void
thread_increment_recent_cpu (void)
{
  /* This function only works when thread_mlfqs is TRUE. */
  ASSERT (thread_mlfqs);

  /* Do NOT calculate the load_avg if the thread is in idle. */
  if (thread_current() == idle_thread) {
    return;
  }

  int32_t r_cpu = thread_current ()->recent_cpu;
  r_cpu = fixpt_real_add (r_cpu, 1);

  thread_current ()->recent_cpu = r_cpu;

}

/* Idle thread.  Executes when no other thread is ready to run.

   The idle thread is initially put on the ready list by
   thread_start().  It will be scheduled once initially, at which
   point it initializes idle_thread, "up"s the semaphore passed
   to it to enable thread_start() to continue, and immediately
   blocks.  After that, the idle thread never appears in the
   ready list.  It is returned by next_thread_to_run() as a
   special case when the ready list is empty. */
static void
idle (void *idle_started_ UNUSED)
{
  struct semaphore *idle_started = idle_started_;
  idle_thread = thread_current ();
  sema_up (idle_started);

  for (;;)
    {
      /* Let someone else run. */
      intr_disable ();
      thread_block ();

      /* Re-enable interrupts and wait for the next one.

         The `sti' instruction disables interrupts until the
         completion of the next instruction, so these two
         instructions are executed atomically.  This atomicity is
         important; otherwise, an interrupt could be handled
         between re-enabling interrupts and waiting for the next
         one to occur, wasting as much as one clock tick worth of
         time.

         See [IA32-v2a] "HLT", [IA32-v2b] "STI", and [IA32-v3a]
         7.11.1 "HLT Instruction". */
      asm volatile ("sti; hlt" : : : "memory");
    }
}

/* Function used as the basis for a kernel thread. */
static void
kernel_thread (thread_func *function, void *aux)
{
  ASSERT (function != NULL);

  intr_enable ();       /* The scheduler runs with interrupts off. */
  function (aux);       /* Execute the thread function. */
  thread_exit ();       /* If function() returns, kill the thread. */
}

/* Returns the running thread. */
struct thread *
running_thread (void)
{
  uint32_t *esp;

  /* Copy the CPU's stack pointer into `esp', and then round that
     down to the start of a page.  Because `struct thread' is
     always at the beginning of a page and the stack pointer is
     somewhere in the middle, this locates the curent thread. */
  asm ("mov %%esp, %0" : "=g" (esp));
  return pg_round_down (esp);
}

/* Returns true if T appears to point to a valid thread. */
static bool
is_thread (struct thread *t)
{
  return t != NULL && t->magic == THREAD_MAGIC;
}

/* Does basic initialization of T as a blocked thread named
   NAME. */
static void
init_thread (struct thread *t, const char *name, int priority)
{
  enum intr_level old_level;

  ASSERT (t != NULL);
  ASSERT (PRI_MIN <= priority && priority <= PRI_MAX);
  ASSERT (name != NULL);

  memset (t, 0, sizeof *t);
  t->status = THREAD_BLOCKED;
  strlcpy (t->name, name, sizeof t->name);
  t->stack = (uint8_t *) t + PGSIZE;
  t->priority = priority;
  t->magic = THREAD_MAGIC;

  /* initialise new fields */
  t->effective_pri = priority;
  list_init(&t->donor_list);
  t->donee_lock = NULL;

  /* Advanced scheduler */
  /* Initlaise the niceness and recent_cpu */
  t->nice = 0;
  t->recent_cpu = real_to_fixpt(0);

  old_level = intr_disable ();
  list_push_back (&all_list, &t->allelem);

  /* Initalise a mmap_list. */
  list_init(&t->mmap_list);

  intr_set_level (old_level);
}

/* Allocates a SIZE-byte frame at the top of thread T's stack and
   returns a pointer to the frame's base. */
static void *
alloc_frame (struct thread *t, size_t size)
{
  /* Stack data is always allocated in word-size units. */
  ASSERT (is_thread (t));
  ASSERT (size % sizeof (uint32_t) == 0);

  t->stack -= size;
  return t->stack;
}

/* Chooses and returns the next thread to be scheduled.  Should
   return a thread from the run queue, unless the run queue is
   empty.  (If the running thread can continue running, then it
   will be in the run queue.)  If the run queue is empty, return
   idle_thread. */
static struct thread *
next_thread_to_run (void)
{
  if (list_empty (&ready_list))
    return idle_thread;
  else
    return list_entry (list_pop_front (&ready_list), struct thread, elem);
}

/* Completes a thread switch by activating the new thread's page
   tables, and, if the previous thread is dying, destroying it.

   At this function's invocation, we just switched from thread
   PREV, the new thread is already running, and interrupts are
   still disabled.  This function is normally invoked by
   thread_schedule() as its final action before returning, but
   the first time a thread is scheduled it is called by
   switch_entry() (see switch.S).

   It's not safe to call printf() until the thread switch is
   complete.  In practice that means that printf()s should be
   added at the end of the function.

   After this function and its caller returns, the thread switch
   is complete. */
void
thread_schedule_tail (struct thread *prev)
{
  struct thread *cur = running_thread ();

  ASSERT (intr_get_level () == INTR_OFF);

  /* Mark us as running. */
  cur->status = THREAD_RUNNING;

  /* Start new time slice. */
  thread_ticks = 0;

#ifdef USERPROG
  /* Activate the new address space. */
  process_activate ();
#endif

  /* If the thread we switched from is dying, destroy its struct
     thread.  This must happen late so that thread_exit() doesn't
     pull out the rug under itself.  (We don't free
     initial_thread because its memory was not obtained via
     palloc().) */
  if (prev != NULL && prev->status == THREAD_DYING && prev != initial_thread)
    {
      ASSERT (prev != cur);
      palloc_free_page (prev);
    }
}

/* Schedules a new process.  At entry, interrupts must be off and
   the running process's state must have been changed from
   running to some other state.  This function finds another
   thread to run and switches to it.

   It's not safe to call printf() until thread_schedule_tail()
   has completed. */
static void
schedule (void)
{
  struct thread *cur = running_thread ();
  struct thread *next = next_thread_to_run ();
  struct thread *prev = NULL;

  ASSERT (intr_get_level () == INTR_OFF);
  ASSERT (cur->status != THREAD_RUNNING);
  ASSERT (is_thread (next));

  if (cur != next)
    prev = switch_threads (cur, next);
  thread_schedule_tail (prev);
}

/* Returns a tid to use for a new thread. */
static tid_t
allocate_tid (void)
{
  static tid_t next_tid = 1;
  tid_t tid;

  lock_acquire (&tid_lock);
  tid = next_tid++;
  lock_release (&tid_lock);

  return tid;
}

struct thread *sema_elem_thread (const struct list_elem *elm) {
  struct semaphore sema = list_entry (elm, struct semaphore_elem, elem) -> semaphore;
  return list_entry (list_front (&sema.waiters), struct thread, elem);
}

bool is_higher_priority(const struct list_elem *t1, const struct list_elem *t2, void *aux UNUSED) {
  struct thread *th1 = list_entry (t1, struct thread, elem);
  struct thread *th2 = list_entry (t2, struct thread, elem);
  return th1 -> effective_pri > th2 -> effective_pri;
}

bool donor_higher_priority(const struct list_elem *t1, const struct list_elem *t2, void *aux UNUSED) {
  struct thread *th1 = list_entry (t1, struct thread, donor_elem);
  struct thread *th2 = list_entry (t2, struct thread, donor_elem);
  return th1 -> effective_pri > th2 -> effective_pri;
}

/* Get the thread associated with the tid from all_list */
struct thread*
thread_from_tid (tid_t tid) 
{
  struct list_elem *e;
  for(e = list_begin(&all_list);
		e != list_end(&all_list);
                e = list_next(e))
  {
    struct thread *t = list_entry(e, struct thread, allelem);
    if (t -> tid == tid)
    {
      return t;
    }
  } 
  
  return NULL;
 
}


/* Offset of `stack' member within `struct thread'.
   Used by switch.S, which can't figure it out on its own. */
uint32_t thread_stack_ofs = offsetof (struct thread, stack);
