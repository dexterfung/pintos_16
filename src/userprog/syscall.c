#include "userprog/syscall.h"
#include <stdio.h>
#include <stdbool.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "userprog/process.h"
#include "threads/vaddr.h"
#include "devices/shutdown.h"
#include "threads/malloc.h"
#include "userprog/pagedir.h"
#include "userprog/exception.h"
#include "threads/synch.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include <user/syscall.h>
#include "vm/page.h"
#include "userprog/exception.h"

/* function declaration */
static void syscall_handler(struct intr_frame *);
//static bool safe_ptr (const void *ptr);
static void *get_arg(struct intr_frame *f, int i);
static void internal_exit(void);

static void sys_halt(struct intr_frame *f);
static void sys_exit(struct intr_frame *f);
static void sys_exec(struct intr_frame *f);
static void sys_wait(struct intr_frame *f);
static void sys_create(struct intr_frame *f);
static void sys_remove(struct intr_frame *f);
static void sys_open(struct intr_frame *f);
static void sys_filesize(struct intr_frame *f);
static void sys_read(struct intr_frame *f);
static void sys_write(struct intr_frame *f);
static void sys_seek(struct intr_frame *f);
static void sys_tell(struct intr_frame *f);
static void sys_close(struct intr_frame *f);

/* For task 3*/
static void sys_mmap(struct intr_frame *f);
static void sys_munmap(struct intr_frame *f);

static struct fd_handler *file_handler_from_fd(int fd);
static int gen_fd(void);
static mapid_t gen_mappingid(void);

void (*fp_syscall[SYS_CLOSE])(struct intr_frame *f);

// //To make sure that only one thread is accessing filesys at a time.
/* class variable */
static int cur_fd = 1;

static mapid_t cur_mapid = 1;

void syscall_init(void)
{
  fp_syscall[SYS_HALT] = sys_halt;
  fp_syscall[SYS_EXIT] = sys_exit;
  fp_syscall[SYS_EXEC] = sys_exec;
  fp_syscall[SYS_WAIT] = sys_wait;
  fp_syscall[SYS_CREATE] = sys_create;
  fp_syscall[SYS_REMOVE] = sys_remove;
  fp_syscall[SYS_OPEN] = sys_open;
  fp_syscall[SYS_FILESIZE] = sys_filesize;
  fp_syscall[SYS_READ] = sys_read;
  fp_syscall[SYS_WRITE] = sys_write;
  fp_syscall[SYS_SEEK] = sys_seek;
  fp_syscall[SYS_TELL] = sys_tell;
  fp_syscall[SYS_CLOSE] = sys_close;

  /* For task 3*/
  fp_syscall[SYS_MMAP] = sys_mmap;
  fp_syscall[SYS_MUNMAP] = sys_munmap;

  intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&filesys_lock);
}

static void
syscall_handler(struct intr_frame *f)
{
  void *esp = f->esp;
  if (!safe_ptr(esp))
  {
    //todo - set esp to a valid location then jump to exit ()
    internal_exit();
  }

  int syscall_num = *((int *)esp);

  if (syscall_num <= SYS_MUNMAP && syscall_num >= SYS_HALT)
  {
    (*fp_syscall[syscall_num])(f);
  }
  else
  {
    printf("undefined syscall number\n");
  }
}

/* return true if the pointer is safe */
bool safe_ptr(const void *ptr)
{
  return (ptr && is_user_vaddr(ptr) && pagedir_get_page(thread_current()->pagedir, ptr) != NULL);
}

/* return the pointer to the i-th arguments of the API system call, index is 0-based */
static void *
get_arg(struct intr_frame *f, int i)
{
  // if not safe pointer goes to sys exit
  void *ptr = (f->esp) + ((i + 1) * STACK_UNIT_SIZE);
  if (!(safe_ptr(ptr)))
  {
    internal_exit();
    // should not reach here as thread should already have been exited
    return NULL;
  }
  return ptr;
}

/* helper function to jump to system exit when encountering a invalid pointer */
static void
internal_exit(void)
{
  thread_current()->proc->pes->exit_status = ERROR_CODE;
  printf("%s: exit(%d)\n", thread_current()->name, ERROR_CODE);
  thread_exit();
}

/* Internal system call */
/* shutdown pintos */
static void
sys_halt(struct intr_frame *f UNUSED)
{
  shutdown_power_off();
}

/* Update the exit status current thread's pes;
   Tell all children threads their parent has been exited
   Print out exit statement */
static void
sys_exit(struct intr_frame *f)
{
  void *esp = f->esp;

  if (!safe_ptr(esp + STACK_UNIT_SIZE))
  {
    *(int *)(esp + STACK_UNIT_SIZE) = ERROR_CODE;
  }
  int status = *(int *)(esp + STACK_UNIT_SIZE);

  if (thread_current()->proc->pes)
  {
    thread_current()->proc->pes->exit_status = status;
  }

  struct thread *t = thread_current();
  struct list_elem *e = list_begin(&t->mmap_list);

  while (!list_empty(&t->mmap_list))
  {
    struct mmap_data *mm_elem = list_entry(e, struct mmap_data, mmap_elem);
    list_remove(&mm_elem->mmap_elem);
    int page = mm_elem->mpage;
    void *address = mm_elem->address;
    while (page > 0)
    {

      struct sup_page_entry *spe = get_page_entry_from_page(address, t->spt);
      if (spe != NULL)
      {
        if (pagedir_is_dirty(t->pagedir, address))
        {
          lock_acquire(&filesys_lock);
          file_write_at(mm_elem->f_reopened, address, PGSIZE, spe->offset);
          lock_release(&filesys_lock);
        }
        delete_page_entry(&spe->spe_elem, t->spt);
        pagedir_clear_page(t->pagedir, address);
      }
      page--;
      address += PGSIZE;
    }

    lock_acquire(&filesys_lock);
    file_close(mm_elem->f_reopened);
    lock_release(&filesys_lock);

    e = list_next(e);
  }

  //free (mm_elem);

  printf("%s: exit(%d)\n", thread_current()->name, status);

  thread_exit(); //terminate the current user prog.
}

/* Execute the cmd line arguments, only return if the children process
   has loaded their executable */
static void
sys_exec(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;
  char *file_name = *(char **)(get_arg(f, 0));
  if (!safe_ptr(file_name))
  {
    internal_exit();
  }

  tid_t tid = process_execute(file_name);
  pid_t pid = (pid_t)tid;

  if (tid == TID_ERROR)
  {
    *eax = ERROR_CODE;
    return;
  }

  // ensure child has loaded thier executable
  sema_down(&thread_from_tid(tid)->proc->sema_loaded);

  if (thread_current()->proc->load_has_failed)
  {
    *eax = ERROR_CODE;
    return;
  }

  *eax = pid;
}

static void
sys_wait(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;

  tid_t tid = *(tid_t *)(get_arg(f, 0));

  if (thread_from_tid(tid))
  {
    if (!thread_from_tid(tid)->parent_thread)
    {
      *eax = ERROR_CODE;
      return;
    }
  }

  int status = process_wait(tid);

  *eax = status;
}

static void
sys_create(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;
  const char *file_name = *(char **)(get_arg(f, 0));
  unsigned init_size = *(unsigned *)(get_arg(f, 1));
  if (!safe_ptr(file_name))
  {
    internal_exit();
  }

  lock_acquire(&filesys_lock);
  *eax = filesys_create(file_name, init_size);
  lock_release(&filesys_lock);
}

static void
sys_remove(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;
  const char *file_name = *(char **)(get_arg(f, 0));
  if (!safe_ptr(file_name))
  {
    internal_exit();
  }

  lock_acquire(&filesys_lock);
  *eax = filesys_remove(file_name);
  lock_release(&filesys_lock);
}

static void
sys_open(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;
  const char *file_name = *(char **)(get_arg(f, 0));
  if (!safe_ptr(file_name))
  {
    internal_exit();
  }

  lock_acquire(&filesys_lock);
  struct file *file = filesys_open(file_name);
  lock_release(&filesys_lock);

  if (!file)
  {
    *eax = ERROR_CODE;
    return;
  }

  struct fd_handler *file_handler = malloc(sizeof(struct fd_handler));
  if (!file_handler)
  {
    printf("error - cannot malloc for file_handler\n");
    internal_exit();
  }
  file_handler->fd = gen_fd();
  file_handler->file = file;
  list_push_back(&thread_current()->proc->fd_list, &file_handler->fd_elem);
  *eax = file_handler->fd;
}

static void
sys_filesize(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;
  int fd = *(int *)(get_arg(f, 0));

  *eax = file_length(file_handler_from_fd(fd)->file);
}

static void
sys_read(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;

  int fd = *(int *)(get_arg(f, 0));
  char *buffer = *(char **)(get_arg(f, 1));
  unsigned size = *(unsigned *)(get_arg(f, 2));

  void *buf = (void *)buffer;
  unsigned s = size;
  while (buf)
  {

    if (!buffer || !is_user_vaddr(buf))
    {
      internal_exit();
    }

    if (buf > (void *)START_UADDR && buf < thread_current()->spt->code)
    {
      internal_exit();
    }

    if (!pagedir_get_page(thread_current()->pagedir, buf))
    {
      struct sup_page_entry *spe = get_page_entry_from_page(pg_round_down(buf), thread_current()->spt);
      if (spe)
      {
        load_data(spe);
      }
      else if (buf >= (f->esp - 32))
      {
        if (!grow_stack_sc(buf))
        {
          internal_exit();
        }
      }
      else
      {
        internal_exit();
      }
    }

    //base case
    if (s == 0)
    {
      break;
    }

    if (s < PGSIZE)
    {
      s = 0;
      buf = buffer + size - 1;
    }
    else
    {
      //when data exceed one page
      buf += PGSIZE;
      s -= PGSIZE;
    }
  }

  if (fd == 0)
  {
    unsigned i;
    for (i = 0; i < size; i++)
    {
#pragma GCC diagnostic ignored "-Wimplicit-function-declaration"
      *(buffer + i) = input_getc();
    }
    *eax = size;
  }
  else if (fd == 1)
  {
    internal_exit();
  }
  else
  {
    struct file *file = file_handler_from_fd(fd)->file;
    // off_t pos = file_tell (file);
    lock_acquire(&filesys_lock);
    *eax = file_read(file, buffer, size);
    lock_release(&filesys_lock);
    // file_seek (file, pos);
  }
}

static void
sys_write(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;

  int fd = *(int *)(get_arg(f, 0));
  void *buffer = *(void **)(get_arg(f, 1));
  unsigned size = *(unsigned *)(get_arg(f, 2));

  void *buf = (void *)buffer;
  unsigned s = size;
  while (buf)
  {

    if (!buffer || !is_user_vaddr(buf))
    {
      internal_exit();
    }

    if (buf > (void *)START_UADDR && buf < thread_current()->spt->code)
    {
      internal_exit();
    }

    if (!pagedir_get_page(thread_current()->pagedir, buf))
    {
      struct sup_page_entry *spe = get_page_entry_from_page(pg_round_down(buf), thread_current()->spt);
      if (spe)
      {
        load_data(spe);
      }
      else if (buf >= ((f->esp) - 32))
      {

        if (!grow_stack_sc(buf))
        {

          internal_exit();
        }
      }
      else
      {
        internal_exit();
      }
    }

    //base case
    if (s == 0)
    {
      break;
    }

    if (s < PGSIZE)
    {
      s = 0;
      buf = buffer + size - 1;
    }
    else
    {
      //when data exceed one page
      buf += PGSIZE;
      s -= PGSIZE;
    }
  }

  if (fd == 0)
  {
    internal_exit();
  }
  else if (fd == 1)
  {
    putbuf((char *)buffer, size);
    *eax = (int)size;
  }
  else
  {
    struct file *file = file_handler_from_fd(fd)->file;
    lock_acquire(&filesys_lock);
    *eax = file_write(file, buffer, size);
    lock_release(&filesys_lock);
  }
}

static void
sys_seek(struct intr_frame *f)
{
  int fd = *(int *)(get_arg(f, 0));
  unsigned pos = *(unsigned *)(get_arg(f, 1));

  file_seek(file_handler_from_fd(fd)->file, pos);
}

static void
sys_tell(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;
  int fd = *(int *)(get_arg(f, 0));

  *eax = file_tell(file_handler_from_fd(fd)->file);
}

static void
sys_close(struct intr_frame *f)
{
  int fd = *(int *)(get_arg(f, 0));
  struct fd_handler *file_handler = file_handler_from_fd(fd);
  if (!file_handler)
  {
    return;
  }
  list_remove(&file_handler->fd_elem);
  lock_acquire(&filesys_lock);
  file_close(file_handler->file);
  lock_release(&filesys_lock);
  free(file_handler);
}

static void
sys_mmap(struct intr_frame *f)
{
  uint32_t *eax = &f->eax;
  int fd = *(int *)(get_arg(f, 0));
  void *addr = *(void **)(get_arg(f, 1));
  struct thread *t = thread_current();

  /* Check if addr is valid */
  if (addr == 0 || pg_ofs(addr) != 0)
  {
    *eax = ERROR_CODE;
    return;
  }

  /* Check if addr is in code segment, data segment or stack. */
  if (addr_in_code_segment(t, addr) || addr_in_data_segment(t, addr) || addr_in_stack(addr))
  {
    *eax = ERROR_CODE;
    return;
  }

  /* Check if fd is not 0 or 1. */
  if (fd == STDOUT_FILENO || fd == STDOUT_FILENO)
  {
    *eax = ERROR_CODE;
    return;
  }

  struct fd_handler *file_handler = file_handler_from_fd(fd);
  if (!file_handler)
  {
    return;
  }

  struct file *reopened_file = file_reopen(file_handler->file);
  if (reopened_file == NULL)
  {
    free(file_handler);
    *eax = ERROR_CODE;
    return;
  }

  off_t flen = file_length(reopened_file);

  if (flen == 0)
  {
    free(file_handler);
    *eax = ERROR_CODE;
    return;
  }

  int page = 0;
  off_t offs = 0;
  void *user_addr = addr;

  while (flen > 0)
  {
    uint32_t read_bytes = flen;

    read_bytes = flen < PGSIZE ? read_bytes : PGSIZE;
    if (!create_spe(user_addr, reopened_file, read_bytes, (PGSIZE - read_bytes), true, offs, MMF, 0, NULL))
    {
      free(file_handler);
      *eax = ERROR_CODE;
      return;
    }

    struct sup_page_entry *spe = get_page_entry_from_page(user_addr, t->spt);
    if (!load_data(spe))
    {
      free(file_handler);
      free(spe);
      *eax = MAP_FAILED;
      return;
    }

    flen -= PGSIZE;
    user_addr += PGSIZE;
    offs += read_bytes;
    page++;
  }

  struct mmap_data *mm_elem = malloc(sizeof(struct mmap_data));
  if (!mm_elem)
  {
    free(file_handler);
    internal_exit();
  }

  mm_elem->map_id = gen_mappingid();
  ;
  mm_elem->mpage = page;
  mm_elem->address = addr;
  mm_elem->f_reopened = reopened_file;

  list_push_back(&t->mmap_list, &mm_elem->mmap_elem);

  *eax = mm_elem->map_id;
}

static void
sys_munmap(struct intr_frame *f)
{
  mapid_t mapping = *(mapid_t *)(get_arg(f, 0));

  unmap_given_mapid(mapping);
}

void unmap_given_mapid(int mapping)
{
  struct thread *t = thread_current();
  struct list_elem *e = list_begin(&t->mmap_list);

  while (!list_empty(&t->mmap_list))
  {
    struct mmap_data *mm_elem = list_entry(e, struct mmap_data, mmap_elem);
    if (mm_elem->map_id == mapping)
    {
      list_remove(&mm_elem->mmap_elem);
      int page = mm_elem->mpage;
      void *address = mm_elem->address;
      while (page > 0)
      {
        struct sup_page_entry *spe = get_page_entry_from_page(address, t->spt);
        if (spe != NULL)
        {
          if (pagedir_is_dirty(t->pagedir, address))
          {
            lock_acquire(&filesys_lock);
            file_write_at(mm_elem->f_reopened, address, PGSIZE, spe->offset);
            lock_release(&filesys_lock);
          }
          delete_page_entry(&spe->spe_elem, t->spt);
          //TODO: DONALD REMOVE FROM SPT AS WELL
          pagedir_clear_page(t->pagedir, address);
        }
        page--;
        address += PGSIZE;
      }

      lock_acquire(&filesys_lock);
      file_close(mm_elem->f_reopened);
      lock_release(&filesys_lock);
      free(mm_elem);
    }
    e = list_next(e);
  }
}

static struct fd_handler *
file_handler_from_fd(int fd)
{
  struct list_elem *e = list_begin(&thread_current()->proc->fd_list);
  for (; e != list_end(&thread_current()->proc->fd_list); e = list_next(e))
  {
    struct fd_handler *file_handler = list_entry(e, struct fd_handler, fd_elem);

    if (file_handler->fd == fd)
    {
      return file_handler;
    }
  }
  internal_exit();
  return NULL;
}

static int
gen_fd()
{
  return ++cur_fd;
}

static mapid_t
gen_mappingid()
{
  return ++cur_mapid;
}

bool addr_in_code_segment(struct thread *t, void *addr)
{
  void *code_seg_end = t->spt->code;
  return addr >= (void *)START_UADDR && addr < code_seg_end;
}

bool addr_in_data_segment(struct thread *t, void *addr)
{
  void *data_seg_end = t->spt->data;
  void *data_seg_start = t->spt->code;
  return addr >= data_seg_start && addr < data_seg_end;
}

bool addr_in_stack(void *addr)
{
  return (addr >= (PHYS_BASE - STACK_LIMIT)) && (addr < PHYS_BASE);
}
