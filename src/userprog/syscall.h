#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include "threads/thread.h"
#include "threads/vaddr.h"
#include "userprog/process.h"
#include <stdbool.h>

#define ERROR_CODE        -1

struct mmap_data {
    int map_id;
    int mpage;
    void *address;
    struct file *f_reopened;
    struct list_elem mmap_elem;
};

void syscall_init (void);
bool safe_ptr (const void *ptr);
void unmap_given_mapid (int map_id);
bool addr_in_code_segment (struct thread *t, void* addr);
bool addr_in_data_segment (struct thread *t, void* addr);
bool addr_in_stack (void* addr);

#endif /* userprog/syscall.h */
