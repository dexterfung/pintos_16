#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "tpid.h"
#include "threads/synch.h"
#include "filesys/file.h"
#include "filesys/file-descriptor.h"

#define STACK_UNIT_SIZE 4

struct pid_exit_status
{
  pid_t pid;
  int exit_status;
  // elem to be put in pes_list
  struct list_elem e_elem;
  // to indicate whether the process has exited
  struct semaphore aliveness;
};

struct process
{
  pid_t pid;
  
  struct file *executable;
  struct list fd_list;

  // list of struct pid_exit_status
  struct list pes_list;
  // to indicate whether the process has loaded its executable
  struct semaphore sema_loaded;
  // pointer to its correspondingg struct pid_exit_status
  struct pid_exit_status *pes;
  // to indicate whether file has been loaded successfully
  bool load_has_failed;
  
  struct lock *parent_lock;
  struct lock own_lock;    
};

struct lock filesys_lock;

void process_init (struct thread *t);
tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);

bool install_page (void *upage, void *kpage, bool writable);

#endif /* userprog/process.h */
