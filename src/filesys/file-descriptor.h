#ifndef FILESYS_FILE_DESCRIPTOR_H
#define FILESYS_FILE_DESCRIPTOR_H

#include "file.h"
#include <list.h>

struct fd_handler {
   
  struct file *file;
  int fd;
  struct list_elem fd_elem;

};


#endif
